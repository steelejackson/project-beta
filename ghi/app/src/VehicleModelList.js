import React, { useEffect, useState } from 'react';

function VehicleModelList() {
    const [vehicleModels, setVehicleModels] = useState([]);

    const fetchVehicleModels = async () => {
        try {
            const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setVehicleModels(data.models);
            } else {
                throw new Error('failed to fetch');
            }
        } catch (error) {
            console.error('error', error);
        }
    };

    useEffect(() => {
        fetchVehicleModels()
    }, []);

    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Vehicle Name</th>
                    <th>Vehicle Photo</th>
                    <th>Vehicle Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {vehicleModels.map(model => {
                    return (
                        <tr key={ model.id }>
                            <td>{ model.name }</td>
                            <td><img src={model.picture_url} style={{ maxWidth: '200px' }} /></td>
                            <td>{ model.manufacturer.name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default VehicleModelList