import React, { useEffect, useState } from 'react';


function SalesForm() {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [autos, setAutos] = useState([]);
    const [vin, setVin] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const fetchAutos = async () => {
        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(autosUrl);
        if (response.ok) {
          const data = await response.json();
          const unsoldAutos = data.autos.filter(auto => !auto.sold);
          setAutos(unsoldAutos);
        }
      }

    const fetchSalespeople = async () => {
      const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
      const response = await fetch(salespeopleUrl);
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      }
    }

    const fetchCustomers = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customersUrl);
        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers);
        }
      }


    useEffect(() => {
      fetchSalespeople();
      fetchCustomers();
      fetchAutos();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.automobile = vin;
      data.salesperson = salesperson;
      data.customer = customer;
      data.price = price;
      console.log(data)

      const salesUrl = `http://localhost:8090/api/sales/`;
      const fetchOptions = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(salesUrl, fetchOptions);
        if (response.ok) {
          setVin('');
          setSalesperson('');
          setCustomer('');
          setPrice('');
        }
    }

    const handleVinChange = (event) => {
      const value = event.target.value;
      setVin(value);
    }

    const handleSalespersonChange = (event) => {
      const value = event.target.value;
      setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
      const value = event.target.value;
      setCustomer(value);
    }

    const handlePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value);
    }

      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a new sale</h1>
              <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="mb-3">
                  <select onChange={handleVinChange} value={vin} required name="vin" id="vin" className="form-select">
                    <option value="">Choose an automobile VIN</option>
                    {autos.map(automobile => {
                      return (
                        <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeople.map(salesperson => {
                      return (
                        <option key={salesperson.href} value={salesperson.id}>{salesperson.first_name + ' ' + salesperson.last_name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                    <option value="">Choose a customer</option>
                    {customers.map(customer => {
                      return (
                        <option key={customer.href} value={customer.id}>{customer.first_name + ' ' + customer.last_name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" id="price" className="form-control" style={{ height: '20px' }} />
                  <label htmlFor="price"style={{ marginTop: '-10px' }}>Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
  }


  export default SalesForm;
