import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function History({ vin }) {
    
    const [appointments, setAppointments] = useState([]);

    console.log(appointments)

    const fetchAppointments = async (vin) => {
        console.log(vin)
        try {
            console.log('vin value:', vin)
            const url = `http://localhost:8080/api/history/${vin}/`;
            const response = await fetch(url); 
            
            console.log(response)
            if (response.ok) {
                const data = await response.json();
                

                
                setAppointments(data.appointments);

            } else {
                throw new Error('failed to fetch');
            }
        } catch (error) {
            console.error('error', error);
        }
    };

   

    useEffect(() => {
        if(vin) {
            fetchAppointments(vin)
        }
       
        
    }, [vin]);

    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Date and Time</th>
                    <th>Assigned Technician Name</th>
                    <th>Reason for Service</th>
                    
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={ appointment.id }>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{ `${appointment.technician.first_name} ${appointment.technician.last_name}` }</td>
                            <td>{ appointment.reason }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default History