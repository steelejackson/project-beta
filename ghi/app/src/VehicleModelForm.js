import React, { useEffect, useState } from 'react';

function VehicleModelForm() {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    };

    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    };

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            name: name,
            picture_url: pictureUrl,
            manufacturer_id: manufacturer
        };

        const vehicleModelUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(vehicleModelUrl, fetchConfig);

        if (response.ok) {
            const newVehicleModel = await response.json();

            console.log(newVehicleModel);

            setName('');
            setPictureUrl('');
            setManufacturer('');
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/manufacturers/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                console.log(data);
                console.log(data.manufacturers);
                setManufacturers(data.manufacturers);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Vehicle Model Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Vehicle Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" />
                            <label htmlFor="pictureUrl">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" value={manufacturer} className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturers.map(manufacturer => (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {`${manufacturer.name}`}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelForm;
