import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Customers </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers" style={{ color: 'green' }}>Customers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/new" style={{ color: 'green' }}>Add a customer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Salespeople </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople" style={{ color: 'green' }}>Salespeople</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/new" style={{ color: 'green' }}>Add a salesperson</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Sales </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales" style={{ color: 'green' }}>Sales</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/new" style={{ color: 'green' }}>Record a new sale</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/history" style={{ color: 'green' }}>Salesperson history</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Appointments </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments" style={{ color: 'green' }}>Appointments</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/new" style={{ color: 'green' }}>Add a Service Appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/history" style={{ color: 'green' }}>See Service History By Vin</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Technicians </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                      <NavLink className="nav-link" to="/technicians/new" style={{ color: 'green' }}>Add a Technician</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Manufacturers </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                      <NavLink className="nav-link" to="/manufacturers" style={{ color: 'green' }}>Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                      <NavLink className="nav-link" to="/manufacturers/new" style={{ color: 'green' }}>Add a Manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Vehicle Models </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                      <NavLink className="nav-link" to="/models" style={{ color: 'green' }}>Models</NavLink>
                </li>
                <li className="nav-item">
                      <NavLink className="nav-link" to="/models/new" style={{ color: 'green' }}>Add a Model</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: 'white' }}> Automobiles </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                      <NavLink className="nav-link" to="/automobiles" style={{ color: 'green' }}>All Automobiles</NavLink>
                </li>
                <li className="nav-item">
                      <NavLink className="nav-link" to="/automobiles/new" style={{ color: 'green' }}>Add an automobile</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
