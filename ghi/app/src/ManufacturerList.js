import React, { useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);
    const fetchManufacturers = async () => {
        try {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            } else {
                throw new Error('failed to fetch');
            }
        } catch (error) {
            console.error('error', error);
        }
    };

    useEffect(() => {
        fetchManufacturers()
    }, []);

    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Manufacturer Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                        <tr key={ manufacturer.id }>
                            <td>{ manufacturer.name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default ManufacturerList