import React, { useEffect, useState } from 'react';

function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState('');

    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    };

    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    };

    const handleDateChange = (e) => {
        const value = e.target.value;
        setDate(value);
    };

    const handleTechnicianChange = (e) => {
        const value = e.target.value;
        setTechnician(value);
    };

    const handleReasonChange = (e) => {
        const value = e.target.value;
        setReason(value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
            data.vin = vin;
            data.customer = customer;
            data.date_time = date;
            data.technician = technician;
            data.reason = reason;
        console.log(data)

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            const newAppointment = await response.json();

            console.log(newAppointment);

            setVin('');
            setCustomer('');
            setDate('');
            setTechnician('');
            setReason('');
        }
  
    };

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            console.log(data.technicians);
            setTechnicians(data.technicians);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin">Vin</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                  <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleDateChange} value={date} placeholder="Date" type="text" name="date" id="date" className="form-control" />
                  <label htmlFor="date">Date</label>
                </div>
                <div className="mb-3">
                                    <select onChange={handleTechnicianChange} required id="technician" name="technician"  value={technician} className="form-select">
                                        <option value="">Choose a Technician</option>
                                        {technicians.map(technician => {
                                            return (
                                                <option key={technician.id} value={technician.id}>
                                                    {`${technician.first_name} ${technician.last_name}`}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleReasonChange} value={reason} placeholder="Reason" type="text" name="reason" id="reason" className="form-control" />
                  <label htmlFor="reason">Reason</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    );
}

export default AppointmentForm;