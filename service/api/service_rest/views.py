from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
import json
from django.views.decorators.http import require_http_methods
# Create your views here.


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        'id',
    ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        'first_name',
        'last_name',
        'id',
    ]
class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
       
        
        'date_time',
        'reason',
        'vin',
        'customer',
        'technician',
        'id',
        
    ]

    encoders = {
        'technician': TechnicianDetailEncoder()
    }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'date_time',
        'reason',
        'vin',
        'customer',
        'technician',
        'id',
    ]

    encoders = {
        'technician': TechnicianDetailEncoder(),
    }

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold',
    ]

@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            { 'technicians': technicians },
            encoder = TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'DELETE'])
def api_show_technician(request, pk):
    if request.method == 'GET':
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            { 'technician': technician },
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )

@require_http_methods(['GET', 'POST'])
def api_list_appointments(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        return JsonResponse(
            { 'appointments': appointments },
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content['technician'])
            content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid Technician ID'},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_appointment(request, pk):
    if request.method == 'GET':
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            {'appointment': appointment },
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            { 'deleted': count > 0 }
        )

@require_http_methods(['GET'])
def api_show_service_history(request, vin):
    try:
        appointments = Appointment.objects.filter(vin=vin)
        return JsonResponse(
            {'appointments': appointments },
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse({'message': 'no service history found for the provided vin'}, status=404)




    

        


