from django.urls import path
from .views import list_salespeople, delete_salesperson, list_customers, delete_customer, list_sales, delete_sale, filter_sales

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:id>/", delete_customer, name="delete_customer"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:id>/", delete_sale, name="delete_sale"),
    path("sales/salesperson/<int:salesperson_id>/", filter_sales, name="filter_sales"),
]
