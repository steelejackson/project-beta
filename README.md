# CarCar

Team:

- Steele - Service, Manufacturers, Vehicle Models
- Julia - Sales, Automobiles

## How to Run this App:

CarCar is an application designed for managing various aspects of an automobile dealership, including inventory, automobile sales, and services.
It consists of three main microservices: Inventory, Services, and Sales, which interact with each other to facilitate dealership operations. Sales and service retrieve information from the inventory domain using a poller to maintain updated data on available autos.

1. Forking and then cloning repository onto local computer with the command: git clone "url"
2. Build and run the project with Docker using following commands:
   docker volume create beta-data
   docker-compose build
   docker-compose up
3. Navigate to and access the project in the browser via http://localhost:3000/

## Diagram:

![Img](/images/carcar.png)

## API Documentation:

### Ports:

\*inventory:
"8100:8000"
host is running on 8100
container is running on 8000

\*sales:
"8090:8000"
host is running on port 8090
container is running on port 8000

\*service:
"8080:8000
host is running on port 8080
container is running on port 8000

### Sales API and URLs

1. Customers:
   a. List Customers | GET | http://localhost:8090/api/customers/
   ![Img](/images/ListCustomers.png)

   b. Create a customer | GET | http://localhost:8090/api/customers/
   ![Img](/images/CreateCustomer.png)

   c. Delete a customer | DELETE | http://localhost:8090/api/customers/id/
   ![Img](/images/DeleteCustomer.png)

2. Salespeople:
   a. List Salespeople | GET | http://localhost:8090/api/salespeople/
   ![Img](/images/ListSalespeople.png)

   b. Create a salesperson | GET | http://localhost:8090/api/salespeople/
   ![Img](/images/CreateSalesperson.png)

   c. Delete a salesperson | DELETE | http://localhost:8090/api/salespeople/id/
   ![Img](/images/DeleteCustomer.png)

3. Sales:
   a. List Sales | GET | http://localhost:8090/api/sales/
   ![Img](/images/ListSales.png)

   b. Create a sale | GET | http://localhost:8090/api/sales/
   ![Img](/images/CreateSale.png)

   \*\*IMPORTANT: for price: please add the price in the following format: $0000(however many numbers).00

   c. Delete a sale | DELETE | http://localhost:8090/api/sales/id/
   ![Img](/images/DeleteSale.png)

   d. Filter sales by salesperson | GET | http://localhost:8090/api/sales/salesperson/id/
   ![Img](/images/FilterSales.png)

4. Appointments:
   a. List Appointments | GET | http://localhost:8080/api/appointments/
   ![Img](/images/list%20appointments.png)

   b. Create an appointment | POST | http://localhost:8080/api/appointments/
   ![Img](/images/create%20appointment.png)

   \*\*IMPORTANT: for the date_time field, please add the date in the following format:
   2024-02-06 12:30:00 or YYYY-MM-DD HH:MM:SS

5. Technicians:
   a. Create a Technician | POST | http://localhost:8080/api/technicians/
   ![Img](images/create%20technician.png)

## Value Objects:

Sales microservice has one value object: AutomobileVO. This value object is necessary for our microservice in order to be getting information about all of the automobiles in our inventory which we can then use for registering new sales. We achieve this by using poller which automatically polls the inventory microservice for data, letting us maintain updated data on available autos.

The Service microservice also has one value object: AutomobileVO. With the help of the poller function, AutomobileVO pulls from the automobile model in the inventory, and updates the current VINs in the inventory. This makes it so that both the 'search by VIN' feature is up to date, and that the 'VIP treatment' visual indicator is accurate.

### Inventory API (Optional) -

- Automobiles:
  a. List Automobiles| GET | http://localhost:8100/api/automobiles/
  ![Img](/images/ListAutomobiles.png)

  b. Create an automobile | GET | http://localhost:8100/api/automobiles/
  ![Img](/images/CreateAutomobile.png)

  c. Delete an automobile| DELETE | http://localhost:8100/api/automobiles/vin/
  ![Img](/images/DeleteAutomobile.png)

  d. Update an automobile | PUT | http://localhost:8100/api/automobiles/vin/
  ![Img](/images/UpdateAutomobile.png)

- Manufacturers:
  a. List Manufacturers | GET | http://localhost:8100/api/manufacturers/
  ![Img](images/list%20manufacturers.png)

  b. Create a manufacturer | POST | http://localhost:8100/api/manufacturers/
  ![Img](images/create%20manufacturer.png)

- Vehicle models:
  a. List vehicle models | GET | http://localhost:8100/api/models/
  ![Img](images/list%20vehicle%20models.png)

  b. Create a vehicle model | POST | http://localhost:8100/api/models/
  ![Img](images/create%20vehicle.png)
